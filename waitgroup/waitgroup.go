package main

import (
	"fmt"
	"sync"
	"time"
)

var wg sync.WaitGroup

func main() {
	//	slice := []int{2, 3, 5, 2, 6, 1, 4}
	//	ans := hello

	wg.Add(3)
	go hello()
	go hello()
	go hello()

	fmt.Println("Main done!")
	wg.Wait()
}

func hello() {
	fmt.Println("Hi!")
	time.Sleep(1 * time.Second)
	wg.Done()
}
